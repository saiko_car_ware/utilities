#include <ros/ros.h>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>

const int OK = 0;
const int NG = 1;

void writeElement(xercesc::DOMElement* element) {
	char* name = xercesc::XMLString::transcode(element->getTagName());
	std::cout << "tag     :" << name << std::endl;
	xercesc::XMLString::release(&name);

	xercesc::DOMNamedNodeMap* map = element->getAttributes();
	for ( XMLSize_t i = 0; i < map->getLength(); i++ ) {
		xercesc::DOMAttr* attr= static_cast<xercesc::DOMAttr*>(map->item(i));
		char* attr_name = xercesc::XMLString::transcode(attr->getName());
		char* attr_value = xercesc::XMLString::transcode(attr->getValue());
		std::cout << attr_name << ": " << attr_value << std::endl;

		xercesc::XMLString::release(&attr_name);
		xercesc::XMLString::release(&attr_value);
	}
}

void writeText(xercesc::DOMText* text) {
	XMLCh* buffer = new XMLCh[xercesc::XMLString::stringLen(text->getData()) + 1];
	xercesc::XMLString::copyString(buffer, text->getData());
	xercesc::XMLString::trim(buffer);
	char* content = xercesc::XMLString::transcode(buffer);
	delete[] buffer;

	std::cout << "content :" << content << std::endl;
	xercesc::XMLString::release(&content);
}

void writeNode(xercesc::DOMNode* node)
{
	assert(node != NULL);

	switch( node->getNodeType() ) {
		case xercesc::DOMNode::ELEMENT_NODE:
			writeElement(static_cast<xercesc::DOMElement*>(node));
			break;

		case xercesc::DOMNode::TEXT_NODE:
			writeText(static_cast<xercesc::DOMText*>(node));
			break;
	}

	xercesc::DOMNode* child = node->getFirstChild();
	while(child) {
		writeNode(child);
		xercesc::DOMNode* next = child->getNextSibling();
		child = next;	
	}
}

int main()
{
    try
    {
        xercesc::XMLPlatformUtils::Initialize();
    }
    catch(const xercesc::XMLException& exp)
    {
        std::cerr << "初期化エラー" << std::endl;
    }
    
    xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
	parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);

    try {
		const char* xmlFile = "/home/autoware/load_data/haneda/haneda_mxl/127945_503170.xml";
		parser->parse(xmlFile);
		xercesc::DOMDocument* dom = parser->getDocument();
		writeNode(dom);
	}
	catch ( const xercesc::XMLException& exp ) {
		char* message = xercesc::XMLString::transcode(exp.getMessage());
		std::cout<< "Exception message is: \n" << message << std::endl;
		xercesc::XMLString::release(&message);
		return NG;
	}
	catch ( const xercesc::DOMException& exp ) {
		char* message = xercesc::XMLString::transcode(exp.getMessage());
		std::cout<< "Exception message is: \n" << message << std::endl;
		xercesc::XMLString::release(&message);
		return NG;
	}
	catch (...) {
		std::cout << "Unexpected Exception" << std::endl;
		return NG;
	}

	xercesc::ErrorHandler* errHandler = new xercesc::HandlerBase();
	parser->setErrorHandler(errHandler);
    return 0;
}