#include <ros/ros.h>
#include <std_msgs/String.h>

struct NodeTime
{
    std::string name;
    ros::Time time;
};

class AliveChecker
{
private:
    ros::NodeHandle nh_, p_nh_;
    ros::Publisher pub_node_dead_;
    ros::Subscriber sub_alive_;

    std::vector<NodeTime> alive_node_list_;

    void callbackAlive(const std_msgs::String &msg)
    {
        for(int i=0; i<alive_node_list_.size(); i++)
        {
            if(alive_node_list_[i].name == msg.data)
            {
                alive_node_list_.erase(alive_node_list_.begin() + i);
                break;
            }
        }

        NodeTime nt = {msg.data, ros::Time::now()};
        alive_node_list_.push_back(nt);
    }
public:
    AliveChecker(ros::NodeHandle nh, ros::NodeHandle p_nh)
        : nh_(nh)
        , p_nh_(p_nh)
    {
        pub_node_dead_ = nh_.advertise<std_msgs::String>("/node_dead", 10);
        sub_alive_ = nh_.subscribe("/alive", 10, &AliveChecker::callbackAlive, this);
    }

    void run()
    {
        std_msgs::String str;
        const ros::Duration time_th = ros::Duration(1.0);
        ros::Time nowtime = ros::Time::now();
        for(NodeTime nt : alive_node_list_)
        {
            ros::Duration time_diff = nowtime - nt.time;
            std::cout << time_diff.sec + time_diff.nsec * 1E-9 << std::endl;
            if(time_diff > time_th)
            {
                str.data = nt.name;
                pub_node_dead_.publish(str);
                return;
            }
        }

        str.data = "";
        pub_node_dead_.publish(str);
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "alive_checker");
    ros::NodeHandle nh, private_nh("~");

    AliveChecker ac(nh, private_nh);
    ros::Rate rate(30);
    while(ros::ok())
    {
        ros::spinOnce();
        ac.run();
        rate.sleep();
    }
    return 0;
}