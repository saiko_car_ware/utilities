#include <ros/ros.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

class TcpCanReceiver
{
private:
    static const int READSIZE_ = 200;
    ros::NodeHandle nh_, p_nh_;

    int socket_handle_;
    bool is_connect_;
public:
    TcpCanReceiver(ros::NodeHandle nh, ros::NodeHandle p_nh, std::string ip, int port)
        : nh_(nh)
        , p_nh_(p_nh)
        , is_connect_(false)
    {
        struct sockaddr_in dstAddr;
        memset(&dstAddr, 0, sizeof(dstAddr));
        dstAddr.sin_port = htons(port);
        dstAddr.sin_family = AF_INET;
        dstAddr.sin_addr.s_addr = inet_addr(ip.c_str());

        socket_handle_ = socket(AF_INET, SOCK_STREAM, 0);
        if(socket_handle_ < 0) return;

        int connect_ret = connect(socket_handle_, (struct sockaddr *) &dstAddr, sizeof(dstAddr));
        if(connect_ret < 0) return;

        is_connect_ = true;
    }

    ~TcpCanReceiver()
    {
    }

    void run()
    {
        unsigned char buf[500];

        while(ros::ok())
        {
            int read_size=read(socket_handle_, &buf, TcpCanReceiver::READSIZE_);
            std::cout << read_size << std::endl;
            for(int j=0;j<read_size;j++) printf("%d,", buf[j]);
            printf("\n");

            for(int ind=0; ind<read_size; ind++)
            {

            }
            ros::spinOnce();
        }
    }

    bool isConnect() {return is_connect_;}
};

int main(int argc, char** argv)
{
   	ros::init(argc, argv, "tcp_prius_can_receiver");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

    std::string ip;
    int port;
	private_nh.param<std::string>("ip", ip, "192.168.1.10");
    private_nh.param<int>("ip", port, 100);
    //std::cout << ip << "," << port << std::endl;

    TcpCanReceiver tcp_can_receiver(nh, private_nh, ip, port);
    if(tcp_can_receiver.isConnect() == false)
    {
        std::cout << "error : not connect" << std::endl;
        return -1;
    }

    tcp_can_receiver.run();
    return 0;
}