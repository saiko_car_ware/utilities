cmake_minimum_required(VERSION 2.8.3)
project(vehicle_socket)


find_package(autoware_build_flags REQUIRED)

find_package(autoware_msgs REQUIRED)

find_package(tablet_socket_msgs REQUIRED)
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  autoware_msgs
  autoware_can_msgs
  tablet_socket_msgs
  autoware_config_msgs
  autoware_system_msgs
  microbus_interface
  nmea_msgs
  tf
  geometry_msgs
  roslib
  rosgraph_msgs
)

set(CMAKE_CXX_FLAGS "-O2 -Wall -fpermissive ${CMAKE_CXX_FLAGS}")

catkin_package(CATKIN_DEPENDS
  roscpp
  std_msgs
  autoware_can_msgs
  autoware_msgs
  tablet_socket_msgs
  autoware_config_msgs
  autoware_system_msgs
  microbus_interface
  nmea_msgs
  tf
  geometry_msgs
  roslib
  rosgraph_msgs
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_executable(vehicle_receiver nodes/vehicle_receiver/vehicle_receiver.cpp)
target_link_libraries(vehicle_receiver ${catkin_LIBRARIES})
add_dependencies(vehicle_receiver
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(vehicle_sender nodes/vehicle_sender/vehicle_sender.cpp)
target_link_libraries(vehicle_sender ${catkin_LIBRARIES})
add_dependencies(vehicle_sender
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(kvaser_microbus_can_sender
	nodes/kvaser_microbus_can_sender/kvaser_microbus_can_sender_stroke.cpp)
target_link_libraries(kvaser_microbus_can_sender
  ${catkin_LIBRARIES}
  canlib)
add_dependencies(kvaser_microbus_can_sender
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(kvaser_microbus_can_sender_old
	nodes/kvaser_microbus_can_sender_old/kvaser_microbus_can_sender_stroke_old.cpp)
target_link_libraries(kvaser_microbus_can_sender_old
  ${catkin_LIBRARIES}
  canlib)
add_dependencies(kvaser_microbus_can_sender_old
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(microbus_log_aggregate
	nodes/kvaser_microbus_can_sender/microbus_log_aggregate.cpp)
target_link_libraries(microbus_log_aggregate
  ${catkin_LIBRARIES}
  canlib)
add_dependencies(microbus_log_aggregate
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(saver
	nodes/kvaser_microbus_can_sender/saver.cpp)
target_link_libraries(saver
  ${catkin_LIBRARIES}
  canlib)
add_dependencies(saver
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(client
	nodes/kvaser_microbus_can_sender/client.cpp)
target_link_libraries(client
  ${catkin_LIBRARIES}
  canlib)
add_dependencies(client
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(kvaser_microbus_can_receiver
	nodes/kvaser_microbus_can_receiver/kvaser_microbus_can_receiver.cpp)
target_link_libraries(kvaser_microbus_can_receiver
  ${catkin_LIBRARIES}
  canlib)
add_dependencies(kvaser_microbus_can_receiver
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(tcp_microbus_can_receiver
	nodes/tcp_microbus_can_receiver/tcp_microbus_can_receiver.cpp)
target_link_libraries(tcp_microbus_can_receiver
  ${catkin_LIBRARIES}
  )
add_dependencies(tcp_microbus_can_receiver
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(ship_steering
	nodes/ship_steering/ship_steering.cpp)
target_link_libraries(ship_steering
  ${catkin_LIBRARIES}
  )
add_dependencies(ship_steering
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(microbus_pseudo_can_publisher
	nodes/microbus_pseudo_can_publisher/microbus_pseudo_can_publisher.cpp)
target_link_libraries(microbus_pseudo_can_publisher
  ${catkin_LIBRARIES}
  )
add_dependencies(microbus_pseudo_can_publisher
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(microbus_pseudo_can_sender
	nodes/microbus_pseudo_can_sender/microbus_pseudo_can_sender.cpp)
target_link_libraries(microbus_pseudo_can_sender
  ${catkin_LIBRARIES}
  )
add_dependencies(microbus_pseudo_can_sender
  ${catkin_EXPORTED_TARGETS}
  )

add_executable(rs232c_microbus_can_receiver
	nodes/rs232c_microbus_can_receiver/rs232c_microbus_can_receiver.cpp)
target_link_libraries(rs232c_microbus_can_receiver
  ${catkin_LIBRARIES}
  )
add_dependencies(rs232c_microbus_can_receiver
  ${catkin_EXPORTED_TARGETS}
  )


add_executable(alive_checker
	nodes/alive_checker/alive_checker.cpp)
target_link_libraries(alive_checker
  ${catkin_LIBRARIES}
  )
add_dependencies(alive_checker
  ${catkin_EXPORTED_TARGETS}
  )

install(TARGETS vehicle_sender vehicle_receiver kvaser_microbus_can_sender saver client kvaser_microbus_can_receiver
                tcp_microbus_can_receiver ship_steering microbus_log_aggregate microbus_pseudo_can_publisher microbus_pseudo_can_sender
                rs232c_microbus_can_receiver alive_checker kvaser_microbus_can_sender_old
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

install(DIRECTORY launch/
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch)

install(DIRECTORY include/
  DESTINATION ${CATKIN_GLOBAL_INCLUDE_DESTINATION}
  PATTERN ".svn" EXCLUDE
)
